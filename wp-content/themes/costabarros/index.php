<?php get_header(); ?>
<div class="menu-total h-100" id="menu-total">



    <div class="links align-items-center justify-content-center">
      <div class="p-2 bd-highlight"><a data-section="hometop">INÍCIO</a></div>
      <div class="p-2 bd-highlight"><a data-section="quemsomos">QUEM SOMOS</a></div>
      <div class="p-2 bd-highlight"><a data-section="atuacao">ÁREA DE ATUAÇÃO</a></div>
      <div class="p-2 bd-highlight"><a data-section="equipe">EQUIPE</a></div>
      <div class="p-2 bd-highlight"><a data-section="blog">BLOG</a></div>
      <div class="p-2 bd-highlight"><a data-section="contato">CONTATO</a></div>
    </div>

</div>
<main role="main">

  <section class="hometop sessao" id="hometop">
    <div class="linhadotempo d-none d-sm-block">
      <div class="circulo-fora"><div class="circulo-dentro"></div><div class="linha"></div></div>
    </div>
    

    <span
          class="rellax estatua img-fluid"
          data-rellax-speed="-1"

          ><img src="<?php bloginfo('template_directory'); ?>/img/estatua.png"></span>


    <div class="container">
      <div class="logo"><img src="<?php bloginfo('template_directory'); ?>/img/logo-branca.png" data-rellax-speed="-3" class="rellax img-fluid rounded mx-auto d-block"></div>
    </div>
  </section>

  <section class="quemsomos sessao" id="quemsomos">
    <div class="linhadotempo d-none d-sm-block">
      <div class="linha"></div><div class="circulo-fora"><div class="circulo-dentro"></div></div>
    </div>
    <span class="ano">Desde 1997</span>
    <!--<div class="lista-ano d-flex align-items-center justify-content-center">
      <ul class="">
        <li><a href="javascript:void(0)" rel="ano_1">2020</a></li>
      </ul>
    </div>-->
    <div class="container d-flex align-items-center justify-content-center h-100">
      <div class="row d-flex justify-content-center">
        <div class="col-12 col-md-6 align-self-center">
          <div class="txt rounded mx-auto d-block">
            <div class="title"><h1>Negócios fortes!</h1></div>
          </div>
        </div>
        <div class="col-12 col-md-5">
            <div id="ano_1">
              <p>A atividade empresarial é o nosso foco. Trabalhamos para melhorá-la com soluções jurídicas criativas, estratégicas e resolutivas para os desafios e riscos que a compõem.</p>
              <p>Por meio de nossa atuação, fomentamos o empreendedorismo e o desenvolvimento dos negócios dos nossos clientes.</p>
              <p>Desde 1997, atendemos empresas de todos os portes, seja com sócios nacionais ou investidores estrangeiros, dos mais diversos segmentos de negócios, com comunicação em vários idiomas.</p>
              <p>Vamos nos conhecer melhor? Continue navegando em nosso site e participando de nossas redes sociais!</p>
            </div>
        </div>
      </div>
    </div>
  </section>

  <section class="proposito" id="proposito">
    <div class="container">
      <p>Desenvolver Pessoas e Empresas para um mundo melhor.</p>
    </div>
  </section>

  <section class="atuacao sessao" id="atuacao">
    
    <div class="container d-flex align-items-center justify-content-center h-100">
      <div class="row">

        <div class="col-6 col-md-6">
          <div class="txt">
            <div class="title"><h4>SEGMENTOS DE MERCADO</h4></div>
		<ul>

                <li>
               	MÉDICO-HOSPITALAR;
                </li>
          	<li>
                ENERGIA;
                </li>
                <li>
                PETRÓLEO;
                </li>
                <li>
                FUTEBOL;
                </li>
                <li>
                INDÚSTRIAS;
                </li>
                <li>
                INVESTIMENTOS ESTRANGEIROS;
                </li>
                <li>
                CONTRUÇÃO CIVIL E INVESTIMENTOS IMOBILIÁRIOS;
                </li>
                <li>
                COMÉRCIO VAREJISTA;
                </li>
                <li>
                TECNOLOGIA;
                </li>
                <li>
                STARTUPS;
                </li>
	  </ul>

	    
          </div>
        </div>
        <div class="col-6 col-md-6">
            <div class="txt">
	    <div class="title"><h4>NOSSA PRÁTICA</h4></div>
		<li>
               	TRIBUTÁRIO;
                </li>
          	<li>
                EMPRESARIAL;
                </li>
                <li>
                TRABALHISTA EMPRESARIAL;
                </li>
                <li>
                PROTEÇÃO DE DADOS PARA EMPRESAS;
                </li>
                <li>
                CONTRATOS;
                </li>
                
	  	</ul>

            
              
            </div>
        </div>
      </div>
    </div>

	  </section>

  <section class="equipe sessao" id="equipe">
    
      <div class="owl-carousel owl-theme owl-loaded">
        <div class="owl-stage-outer">
            <div class="owl-stage">

                <div class="owl-item item-equipe">
                	<button type="button" class="informacoes" data-toggle="modal" data-target="#item-modal-1" value="testeeee">
        					  Costa Barros
        					</button>
                  <span class="foto" style="background: url('<?php bloginfo('template_directory'); ?>/img/tmp/IMG_9734.png'); background-position: center; background-size: auto 100%">
                  </span>
                </div>

                <div class="owl-item item-equipe">

			<button type="button" class="informacoes" data-toggle="modal" data-target="#item-modal-18" value="testeeee">
        					  Luana Marina
        					</button>
                  <span class="foto" style="background-image: url('<?php bloginfo('template_directory'); ?>/img/tmp/IMG_9711.png'); background-position: center; background-size: auto 100%">
                  </span>
                </div>


                <div class="owl-item item-equipe">
                  <button type="button" class="informacoes" data-toggle="modal" data-target="#item-modal-5" value="testeeee">
                    José Antônio
                  </button>
                  <span class="foto" style="background-image: url('<?php bloginfo('template_directory'); ?>/img/tmp/IMG_9688.png'); background-position: center; background-size: auto 100%">
                  </span>
                </div>

            </div>
        </div>
        <div class="owl-nav">
            <div class="owl-prev"></div>
            <div class="owl-next"></div>
        </div>
    </div>
    
<div class="modal fade" id="item-modal-18" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">LUANA MARINA</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body txtcurriculo">
        <p>Natal/RN | +55 (84) 3311-5900</p>
	<p>Contato pessoal| +55 (84) 9102-1503</p>    
        <p>E-mail: luanaqueiroz@costabarros.com.br</p>
        <h5>Formação Acadêmica</h5>
        <p>Advogada trabalhista empresarial</p>
	<p>Pós-Graduada em Direito do Trabalho e Processo do Trabalho - PUC-MG</p>
	<p>Graduação em direito – UFRN</p>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="item-modal-5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">JOSÉ ANTÔNIO</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body txtcurriculo">
	<p>Contato pessoal | +55 (84) 8849-3190
	<p>José Antonio da Silva Junior - Advogado empresarial desde junho de 2012<p>
        <h5>Formação Acadêmica</h5>
        <p>Pós-graduado em Direito Civil- Teoria Geral e Contratos (FAMEESP)</p>
	<p>Pós-graduado em Direito Tributário - UFRN.</p>
	<p>Graduado em Direito - UFRN.</p>
      </div>
    </div>
  </div>
</div>

  </section>

  <section class="blog sessao" id="blog">
    <div class="container d-flex align-items-center justify-content-center h-100">
      <div class="row">
        <div class="col-12">
          <div class="title"><h1>Nosso Blog</h1></div>
        </div>
          <?php
          $args = array( 'numberposts' => 3, 'order'=> 'ASC', 'orderby' => 'title' );
          $postslist = get_posts( $args );
          foreach ($postslist as $post) :  setup_postdata($post);
            $categoria = get_the_category();
            $nomeCategoria = $categoria[0]->cat_name;
          ?>
              <div class="col-12 col-md-4">
                <div class="card">
                  <!-- <div class="categoria"><?php echo $nomeCategoria; ?></div> -->
                  <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><img src="<?php the_post_thumbnail_url(); ?>" class="card-img-top"></a>
                  <div class="card-body">
                    <h5 class="card-title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h5>
                    <!-- <p class="card-text"><?php the_excerpt(); ?></p> -->
                  </div>
                </div>
               </div>
          <?php endforeach; ?>
      </div>
    </div>
  </section>

  
</main>


<?php get_footer(); ?>