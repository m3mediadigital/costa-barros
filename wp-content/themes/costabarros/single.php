<?php get_header(); ?>
<div class="menu-total h-100" id="menu-total">

    <div class="links align-items-center justify-content-center">
      <div class="p-2 bd-highlight"><a href="<?php echo home_url(); ?>/#hometop">INÍCIO</a></div>
      <div class="p-2 bd-highlight"><a href="<?php echo home_url(); ?>/#quemsomos">QUEM SOMOS</a></div>
      <div class="p-2 bd-highlight"><a href="<?php echo home_url(); ?>/#atuacao">ÁREA DE ATUAÇÃO</a></div>
      <div class="p-2 bd-highlight"><a href="<?php echo home_url(); ?>/#equipe">EQUIPE</a></div>
      <div class="p-2 bd-highlight"><a href="<?php echo home_url(); ?>/#blog">BLOG</a></div>
      <div class="p-2 bd-highlight"><a href="<?php echo home_url(); ?>/#contato">CONTATO</a></div>
    </div>

</div>

<main role="main">

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<section class="blogtop" id="blogtop">
    <div class="container">
      <h1 class="entry-title"><?php the_title(); ?></h1>
      <p><?php if ( ! is_search() ) { get_template_part( 'entry', 'meta' ); } ?></p>
    </div>
</section>

<section class="blogconteudo" id="blogconteudo">
    <div class="container">
      <div class="row">
      	<div class="col-12">
      		<?php get_template_part( 'entry', ( is_front_page() || is_home() || is_front_page() && is_home() || is_archive() || is_search() ? 'summary' : 'content' ) ); ?>
      	</div>
      	</div>
      </div>
    </div>
</section>



</article>

<?php // get_template_part( 'entry' ); ?>
<?php // if ( comments_open() && ! post_password_required() ) { comments_template( '', true ); } ?>
<?php endwhile; endif; ?>
</main>
<?php // get_sidebar(); ?>
<?php get_footer(); ?>