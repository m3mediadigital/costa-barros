<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="NovaM3">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/owl.carousel.css">

    <?php
      wp_head();
	  ?>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;400;700&display=swap" rel="stylesheet">

    <!-- Favicons -->
<link rel="apple-touch-icon" href="" sizes="180x180">
<link rel="icon" href="" sizes="32x32" type="image/png">
<link rel="icon" href="" sizes="16x16" type="image/png">
<link rel="mask-icon" href="" color="">
<link rel="icon" href="">


</head>
<body <?php body_class(); ?>>

<nav class="navbar navbar-default fixed-top">
    <div class="navbar-brand">
      <a href="#" class="logo-pequena">
        <img src="<?php bloginfo('template_directory'); ?>/img/logo-branca.png" height="30" alt="">
      </a>
    </div>
    <div class="menu-h">
      <input type="checkbox" id="xmenu">
      <label for="xmenu">
        <span></span>
        <span></span>
        <span></span>
      </label>
    </div>
</nav>

