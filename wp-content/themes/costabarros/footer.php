<footer class="rodape sessao" id="contato">



      <div class="container d-flex align-items-center justify-content-center h-100">
        <div class="row justify-content-between">
          <div class="col-lg-6">
            <div class="form">
              <h1>Fale Conosco</h1>
              <p>Preencha os campos abaixo para entrar em contato com Costa Barros:</p>
              <form>
                <div class="form-group">
                  <input type="text" class="form-control" id="#" placeholder="Nome">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="#" placeholder="Email">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="#" placeholder="Telefone">
                </div>
                <div class="form-group">
                  <textarea class="form-control" id="#" rows="3">Comentário</textarea>
                </div>
                
                <button type="submit" class="btn btn-primary float-right">ENVIAR</button>
              </form>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="informacoes">

              <div class="media">
                <img class="mr-3" src="<?php bloginfo('template_directory'); ?>/img/icon-fone.png" alt="">
                <div class="media-body align-self-center">
                  <h5 class="mt-0">+ 55 84 3311-5900</h5>
                </div>
              </div>

              <div class="media">
                <img class="mr-3" src="<?php bloginfo('template_directory'); ?>/img/icon-mail.png" alt="">
                <div class="media-body align-self-center">
                  <h5 class="mt-0">contato@costabarros.com.br</h5>
                </div>
              </div>

              <div class="media">
                <img class="mr-3" src="<?php bloginfo('template_directory'); ?>/img/icon-endereco.png" alt="">
                <div class="media-body align-self-center">
                  <h5 class="mt-0"><span>Rua da Bronzita, 1917, Studio 1917, Sala 09.</span><span> Lagoa Nova - Natal - Rio Grande do Norte</span><span>CEP: 59076-500</span></h5>
                </div>
              </div>

              <div class="media">
		<div class="social"><a href="https://www.linkedin.com/company/costa-barros-advogados"><img class="mr-3" src="<?php bloginfo('template_directory'); ?>/img/icon-linkedin.png" alt=""></a></div>
		<div class="social"><a href="https://www.instagram.com/costabarrosadvogados/"><img class="mr-3" src="<?php bloginfo('template_directory'); ?>/img/icon-instagram.png" alt=""></a></div>
                <div class="social"><a href="https://www.facebook.com/costabarrosadv/"><img class="mr-3" src="<?php bloginfo('template_directory'); ?>/img/icon-facebook.png" alt=""></a></div>
                
                
              </div>

            </div>
          </div>
        </div>
    </div>

    
    
  </footer>


<div class="modal fade" id="item-modal-1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle"  class="tlcurriculo">FRANCISCO DE ASSIS COSTA BARROS</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body txtcurriculo">
        <p>Natal/RN | +55 (84) 3311-5900</p>
        <p>Contato Pessoal | +55 (84) 99921-0005</p>
        <p>E-mail: costabarros@costabarros.com.br</p>
        <h5>Formação Acadêmica</h5>
        <p>Pós- Graduação em Gestão de Negócios – UFRN</p>
        <p>Pós- Graduação em Globalização e Comércio Exterior – UFRN</p>
        <p>Pós- Graduação em Direito do Trabalho – UFRN</p>
        <p>Bacharelado em Direito – UFRN</p>
        <p>Membro da Ordem dos Advogados do Brasil</p>
        <p>Seccional do Rio Grande do Norte, sob o número 2.469.</p>
        <h5>Idiomas Estrangeiros</h5>
        <p>Inglês, Francês e Espanhol.</p>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="item-modal-2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle"  class="tlcurriculo">FERNANDA ERIKA SANTOS DA COSTA</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body txtcurriculo">
        <p>Natal/RN | +55 (84) 3311-5900</p>
        <p>Contato Pessoal | +55 (84) 98855-8991</p>
        <p>E-mail: fernandasantos@costabarros.com.br</p>
        <h5>Formação Acadêmica</h5>
        <p>Pós- Graduação em Direito do Trabalho – UFRN</p>
        <p>Graduação em Direito – UFRN</p>
        <h5>Membro da Ordem dos Advogados do Brasil</h5>
        <p>Seccional do Rio Grande do Norte, sob o no 4.581.</p>
        <h5>Idiomas Estrangeiros</h5>
        <p>Inglês.</p>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="item-modal-2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle"  class="tlcurriculo">JOSÉ ANTONIO DA SILVA JUNIOR</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body txtcurriculo">
        <p>Natal/RN | +55 (84) 3311-5900</p>
        <p>Contato Pessoal | +55 (84) 98849-3190</p>
        <p>E-mail: joseantonio@costabarros.com.br</p>
        <h5>Formação Acadêmica</h5>
        <p>Pós-graduação em Direito Tributário – UFRN</p>
        <p>Graduação em Direito – UFRN</p>
        <p>Graduação em Jornalismo – UFRN</p>
        <h5>Membro da Ordem dos Advogados do Brasil</h5>
        <p>Seccional do Rio Grande do Norte, sob o número 10.811.</p>
        <h5>Idiomas Estrangeiros</h5>
        <p>Inglês.</p>
      </div>
    </div>
  </div>
</div>



<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/rellax.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/owl.carousel.js"></script>
<script>
$(document).ready(function(){


  $('.lista-ano a').on('click', function(){
     var target = $(this).attr('rel');
     $("#"+target).show().siblings("div").hide();
  });

  $(window).on('scroll', function() {

    aturapage = $(".hometop").height()

    if($(window).scrollTop() > aturapage) {
      $("body.home .logo-pequena").css("display", "block");
      $(".navbar").addClass("bg-barra");
    }else{
      $("body.home .logo-pequena").css("display", "none");
      $(".navbar").removeClass("bg-barra");
    }

  });

  var rellax = new Rellax('.rellax', {
    speed: -2,
    center: false,
    wrapper: null,
    round: true,
    vertical: true,
    horizontal: false
  });

  $(function(){
    var contador = 0;
    $("#xmenu").click(function(){
      if(contador == 0)
      {
        $(".menu-total").css("display", "flex");
        contador = 1;
      }
      else
      {
        $(".menu-total").css("display", "none");
        contador = 0;
      }
    });
  });
  $("#xmenu").on("click", function(){
    $("#menu-total").toggle();
    $(".navbar-brand a").toggle();
  });


  $('.owl-carousel').owlCarousel({
    loop:true,
    margin:0,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:2,
            nav:false
        },
        1000:{
            items:3,
            nav:true,
            loop:false
        }
    }
  })

  // animação scroll quando se clica no menu
  $('.links a').on('click', function() {
    $(".menu-total").css("display", "none");
    contador = 0;
    $("html, body").animate({
      scrollTop: $('#' + $(this).data('section')).offset().top // ir para a secção cujo o id é o valor do atributo `data-section` do item do menu onde clicamos
    }, 500);
  });

  // guardamos todas as distancias de cada secção até ao todo da página e respetivo id
  var alturas = {};
  $('.section').each(function () {
    alturas[$(this).prop('id')] = $(this).offset().top; // ex: alturas['section_2'] = 600
  });

  // quando fazemos scoll vamos percorrer o nosso obj alturas e comparar a altura de cada secção com o que já andamos em scroll
  $(window).on('scroll', function() {
    for(var seccao in alturas) {
      if($(window).scrollTop() >= alturas[seccao]) {
        $('span').removeClass('active'); // removemos a classe ative
        $('span[data-section="' +seccao+ '"]').addClass('active'); // adicionamos a class active ao item do menu cuja data-section é igual ao id da secção que está a uma maior ou igual distancia do topo do que aquela que percorremos com o scroll
      }
    }
  });

  $('#myModal').on('shown.bs.modal', function () {
    $('#myInput').trigger('focus')
  });
      

});

</script>
<?php wp_footer(); ?>
</body>
</html>
