=== Admin Customizer ===
Contributors: Andolasoft
Tags: admin login page, custom admin login, customize wordpress login, gutenberg, white label
Requires at least: 4.6
Tested up to: 5.5.3
Stable tag: 2.1.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html


== Description ==

Admin Customizer plugin gives ability to customize your WordPress admin login page according to you. Create unique login design or admin login design with admin custom login plugin, Almost every element of login page is customize-able with admin custom login plugin. Design beautiful and eye catching login page styles in few Minutes .

This plugin allows you to easily customize the WordPress Login Screen and Admin Dashboard. This facilitates of uploading your company logo to the login screen, add/change background image, change background colors, styles, hide dashboard widgets, add custom CSS and JS, etc. You can modify the look-n-feel of the login page and Dashboard completely!

Admin Customizer lets the administrators to change the WordPress branding to bring client’s websites to live! You can add a customed logo, custome the login page, create a complete custom WordPress dashboard with widget you like to have and other features to help clients use WordPress.

Admin Customizer plugin is for developers who want to give their clients a more personalised and less confusing content management system. Impress your customers by creating a fully customized WordPress admin. Always professional and superior to regular wordpress websites.

============== Installation Process=====================

* In the WordPress Dashboard go to "Plugins" -> "Add Plugin".
* Search for "Admin Customizer".
* Install the plugin by pressing the "Install" button.
* Activate the plugin by pressing the "Activate" button.

==============Features:==============

* Change background image/color of the default Login Screen.
* Add Images using WordPress media.
* Replace the WordPress Logo on the Login page with your own logo.
* Pick a color using awesome jQuery color picker.
* Customize your WordPress dashboard with your custom dashboard logo and sidebar colors.
* Remove dashboard widgets from the dashboard page.
* Add your custom CSS and JS.



==============Login Section Features:==============

* Change the default WordPress login page logo.
* Customize login logo image Height, Width, Image hover title, Link.
* Customize the login page background image and UI changes.
* Ability to update the background color with color picking option.
* Customize the login page 'lost your password?' text.

==============Dashboard Section Features:==============

* Add your custom logo to the header of the WordPress dashboard.
* Change Custom Howdy admin Text.
* Customize dashboard sidebar color with color picking option.
* Customize dashboard topbar color with color picking option.
* Customize dashboard Submenu color with color picking option.

==============Dashboard Widgets Section Features:==============

* Option to hide the Quick Draft dashboard widget.
* Option to hide activity dashboard widget.
* Option to hide  WordPress Site News and meetups widget


==============Custom CSS Section ==============
* Add your custom CSS here.


==============Custom JS Section ==============
* Add your custom JS here.


== Frequently Asked Questions ==

= Who is this plugin for?  =

This plugin is for everyone, even a normal user can use it easily.

= Can I modify my WordPress login screen using this plugin  =

Yes, Using this plugin, you can create some stunning login screens.

= Can I modify my WordPress dashboard screen using this plugin  =

Yes, Using this plugin, you can completely customize the dashboard's look and feel.

= Can I add custom JS and CSS using this plugin  =


Yes, Using this plugin, you can add custom JS and CSS.




================ Screenshots ========================

1. Customized Login Screen.
2. Option to customize your Login Screen.
3. Option to customize your Dashboard Section.
4. Option to manage Dashboard Widgets.
5. Option to add your own CSS.
6. Option to add own JS.
7. Adds an 'AS Admin Customizer' option in the settings.
